using Application.SteamAuth.Interfaces;
using Application.SteamAuth.Models;

namespace Application.SteamAuth.Services;

public class SteamAuthService
{
    private readonly ISteamAuthClient _steamAuthClient;

    public SteamAuthService(ISteamAuthClient steamAuthClient)
    {
        _steamAuthClient = steamAuthClient;
    }
    
    public async Task<SteamAuthModel> Authenticate(IDictionary<string, string> queryParams)
    {
        queryParams["openid.mode"] = "check_authentication";
        
        string? authenticationResult = await _steamAuthClient.Authenticate(queryParams);

        if (authenticationResult is null)
        {
            return new SteamAuthModel(false, null);
        }

        bool isAuthSuccessfull = authenticationResult.Split("is_valid:")[1].Trim() == "true";
        string? steamUserId = queryParams["openid.identity"].Split('/').LastOrDefault();

        return new SteamAuthModel(isAuthSuccessfull, steamUserId);
    }
}