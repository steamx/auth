﻿namespace Application.SteamAuth.Interfaces;

public interface ISteamAuthClient
{
    Task<string?> Authenticate(IDictionary<string, string> queryParams);
}