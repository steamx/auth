namespace Application.SteamAuth.Models;

public record SteamAuthModel(bool IsAuthenticated, string? UserSteamId);