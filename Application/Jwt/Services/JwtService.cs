using Application.Options;
using JWT.Algorithms;
using JWT.Builder;
using Microsoft.Extensions.Options;

namespace Application.Jwt.Services;

public class JwtService
{
    private readonly JwtOptions _jwtOptions;

    public JwtService(IOptions<JwtOptions> jwtOptions)
    {
        _jwtOptions = jwtOptions.Value;
    }
    
    public string Generate(string userSteamId)
    {
        return new JwtBuilder()
            .WithAlgorithm(new HMACSHA256Algorithm())
            .WithSecret(_jwtOptions.Secret)
            .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(4).ToUnixTimeSeconds())
            .AddClaim("userSteamId", userSteamId)
            .Encode();
    }
}