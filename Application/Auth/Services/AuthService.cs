using Application.Jwt.Services;
using Application.SteamAuth.Models;
using Application.SteamAuth.Services;

namespace Application.Auth.Services;

public class AuthService
{
    private readonly SteamAuthService _steamAuthService;
    private readonly JwtService _jwtService;

    public AuthService(SteamAuthService steamAuthService, JwtService jwtService)
    {
        _steamAuthService = steamAuthService;
        _jwtService = jwtService;
    }
    
    public async Task<string?> Authenticate(Dictionary<string, string> steamAuthQueryParams)
    {
        SteamAuthModel steamAuthModel = await _steamAuthService.Authenticate(steamAuthQueryParams);

        if (!steamAuthModel.IsAuthenticated || steamAuthModel.UserSteamId is null)
        {
            return null;
        }

        return _jwtService.Generate(steamAuthModel.UserSteamId!);
    }
}