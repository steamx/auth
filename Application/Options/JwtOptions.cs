namespace Application.Options;

public record JwtOptions
{
    public const string Jwt = "Jwt";

    public string Secret { get; set; } = String.Empty;
}