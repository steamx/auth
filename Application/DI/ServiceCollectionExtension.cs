using Application.Auth.Services;
using Application.Jwt.Services;
using Application.SteamAuth.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Application.DI; 

public static class ServiceCollectionExtension
{
    public static void AddApplicationLayer(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<SteamAuthService>();
        serviceCollection.AddSingleton<JwtService>();
        serviceCollection.AddSingleton<AuthService>();
    }
}