# Auth

## About
The authentication services that utilize steam openID 2.0 to authenticate users. After successful authentication it generates JWT token for future authorization.

## Endpoints

`[GET] /steamauth` => `plain/text`  
The endpoint accepts all query parameters from steam openID 2.0 response and use them to verify if the
user has logged in successfully or not. Then it generates the JWT token for future authorization. The JWT is returned as plain/text.

The JWT is valid for 4 hours.

## How to run
`docker build -t auth .`  
`docker run -p 8080:80 auth`

To run whole steamx application check `Infrastructure` repository