using Application.SteamAuth.Interfaces;
using RestSharp;

namespace Infrastructure.Rest;

public class SteamAuthClient : ISteamAuthClient
{
    private readonly IRestClient _client;

    public SteamAuthClient()
    {
        _client = new RestClient("https://steamcommunity.com");
    }
    
    public async Task<string?> Authenticate(IDictionary<string, string> queryParams)
    {
        var request = new RestRequest("/openid/login");

        foreach (var (key, value) in queryParams)
        {
            request.AddQueryParameter(key, value);
        }

        var a = (await _client.GetAsync(request));

        return a.Content;
    }
}