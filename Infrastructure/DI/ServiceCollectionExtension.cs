using Application.SteamAuth.Interfaces;
using Infrastructure.Rest;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.DI; 

public static class ServiceCollectionExtension
{
    public static void AddInfrastructureLayer(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<ISteamAuthClient, SteamAuthClient>();
    }
}