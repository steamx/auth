using Application.Auth.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("SteamAuth")]
public class SteamAuthController : ControllerBase
{
    private readonly AuthService _authService;

    public SteamAuthController(AuthService authService)
    {
        _authService = authService;
    }
    
    [HttpGet]
    public async Task<ActionResult<string>> SteamOpenIdAuth()
    {
        var queryParams = Request.Query.Aggregate(new Dictionary<string, string>(), (acc, next) =>
        {
            acc.Add(next.Key, next.Value.ToString());
            
            return acc;
        });

        string? jwt = await _authService.Authenticate(queryParams);

        if (jwt is null)
        {
            return BadRequest();
        }

        return Ok(jwt);
    }
}